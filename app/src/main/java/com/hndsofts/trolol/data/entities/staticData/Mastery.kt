package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafhack on 10/24/2017.
 */

class Mastery {

    var prereq: String = ""
    var name: String = ""
    var ranks: Int = 0
    var image: Image? = null
    var id: Int = 0

}