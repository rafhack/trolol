package com.hndsofts.trolol.data.entities.match

import com.hndsofts.trolol.data.entities.GameParticipant

/**
 * Created by Rafa on 10/23/2017.
 */
class MatchParticipant : GameParticipant() {

    var stats: ParticipantStats = ParticipantStats()
    var masteries: ArrayList<MatchMastery> = ArrayList()
    var participantId: Int = 0

}