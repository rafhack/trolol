package com.hndsofts.trolol.data.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * Created by Rafa on 5/25/2017.
 */

open class Summoner : Serializable {

    var profileIconId: Int = 0
    @SerializedName(value = "name", alternate = arrayOf("summonerName"))
    var name: String = ""
    var summonerLevel: Long = 0
    var revisionDate: Calendar? = null
    @SerializedName(value = "id", alternate = arrayOf("summonerId"))
    var id: Long = 0
    var accountId: Long = 0
}
