package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafhack on 10/24/2017.
 */
class StaticDataPack(var champions: StaticData<Champion>?, var spells: StaticData<SummonerSpell>?,
                     var masteries: StaticData<Mastery>?, var items: StaticData<Item>?)