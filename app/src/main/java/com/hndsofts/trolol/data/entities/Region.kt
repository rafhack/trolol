package com.hndsofts.trolol.data.entities

/**
 * Created by Rafa on 5/30/2017.
 */
enum class Region constructor(endpoint: String, name: String) {
    BR("br1", "BR"),
    EUNE("eun1", "EUNE"),
    EUW("euw1", "EUW"),
    JP("jp1", "JP"),
    KR("kr", "KR"),
    LAN("la1", "LAN"),
    LAS("la2", "LAS"),
    NA("na1", "NA"),
    OCE("oc1", "OCE"),
    TR("tr1", "TR"),
    RU("ru", "RU"),
    PBE("pbe1", "PBE");

    var endPoint: String internal set
    var regionName: String internal set

    init {
        this.endPoint = endpoint
        this.regionName = name
    }

    companion object {
        fun from(findValue: String): Region = Region.values().first { it.regionName == findValue }
    }
}