package com.hndsofts.trolol.data.entities

/**
 * Created by Rafhack on 5/29/2017.
 */
class MiniSerie {

    var wins: Int = 0
    var losses: Int = 0
    var target: Int = 0
    var progress: String = ""

}