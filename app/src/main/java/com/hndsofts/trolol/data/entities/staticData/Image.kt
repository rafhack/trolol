package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafhack on 5/31/2017.
 */
class Image {
    var full: String = ""
    var group: String = ""
    var sprite: String = ""
}