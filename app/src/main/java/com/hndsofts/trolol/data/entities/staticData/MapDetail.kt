package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafhack on 5/31/2017.
 */
class MapDetail {
    var mapName: String = ""
    var image: Image? = null
    var mapId: Long = 0
}