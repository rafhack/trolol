package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafa on 6/5/2017.
 */
class SummonerSpell {

    var id: Long = 0
    var image: Image? = null
    var key: String = ""
    var name: String = ""

}