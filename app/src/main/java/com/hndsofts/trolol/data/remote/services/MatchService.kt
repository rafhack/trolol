package com.hndsofts.trolol.data.remote.services

import com.google.gson.JsonObject
import com.hndsofts.trolol.data.entities.match.Match
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Single

/**
 * Created by Rafa on 10/23/2017.
 */
interface MatchService {

    @GET("match/v3/matchlists/by-account/{accountId}")
    fun getRecentMatchList(@Path("accountId") accountId: Long?, @Query("endIndex") endIndex: Int?): Single<JsonObject>

    @GET("match/v3/matches/{matchId}")
    fun getMatchDetail(@Path("matchId") gameId: Long): Single<Match>

}