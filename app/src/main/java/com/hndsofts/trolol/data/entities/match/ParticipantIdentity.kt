package com.hndsofts.trolol.data.entities.match

/**
 * Created by Rafa on 10/23/2017.
 */
class ParticipantIdentity {

    var player: Player = Player()
    var participantId: Int = 0

}