package com.hndsofts.trolol.data.entities.match

import com.hndsofts.trolol.data.entities.BannedChampion

/**
 * Created by Rafa on 10/23/2017.
 */

class TeamStats {

    var firstDragon: Boolean = false
    var firstInhibitor: Boolean = false
    var bans: List<BannedChampion> = ArrayList()
    var baronKills: Int = 0
    var firstRiftHerald: Boolean = false
    var firstBaron: Boolean = false
    var riftHeraldKills: Int = 0
    var firstBlood: Boolean = false
    var teamId: Int = 0
    var firstTower: Boolean = false
    var vilemawKills: Int = 0
    var inhibitorKills: Int = 0
    var towerKills: Int = 0
    var win: String = ""
    var dragonKills: Int = 0


}