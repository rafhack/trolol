package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafhack on 10/24/2017.
 */
class Item {

    var id: Int = 0
    var image: Image? = null
    var name: String = ""

}