package com.hndsofts.trolol.data.entities.match

/**
 * Created by Rafa on 10/23/2017.
 */

class Player {

    var currnetPlatformId: String = ""
    var summonerName: String = ""
    var matchHistoryUri: String = ""
    var platformId: String = ""
    var currentAccountId: Long = 0
    var profileIcon: Int = 0
    var summonerId: Long = 0
    var accountId: Long = 0

}