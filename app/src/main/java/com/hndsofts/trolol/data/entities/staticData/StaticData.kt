package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafa on 5/30/2017.
 */
class StaticData<T> {
    var data: Map<Long, T?>? = null
}