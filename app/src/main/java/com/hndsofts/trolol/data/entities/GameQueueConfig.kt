package com.hndsofts.trolol.data.entities

/**
 * Created by Rafa on 5/31/2017.
 */
enum class GameQueueConfig constructor(configId: Int, configName: String) {
    CUSTOM(0, "Custom"),
    NORMAL_3x3(8, "Normal 3v3"),
    NORMAL_5x5_BLIND(2, "Normal 5v5 Blind Pick"),
    NORMAL_5x5_DRAFT(14, "Normal 5v5 Draft Pick"),
    RANKED_SOLO_5x5(4, "Ranked Solo 5v5"),
    RANKED_FLEX_TT(9, "Ranked Flex"),
    RANKED_TEAM_5x5(42, "Ranked Team 5v5"),
    ARAM_5x5(450, "ARAM"),
    URF_5x5(76, "Ultra Rapid Fire"),
    ASCENSION_5x5(96, "Ascension"),
    TEAM_BUILDER_DRAFT_UNRANKED_5x5(400, "Normal 5v5 Draft Pick"),
    TEAM_BUILDER_RANKED_SOLO(420, "Team ranked solo"),
    RANKED_FLEX_SR(440, "Ranked Flex"),
    COSMIC_RUINS(610, "Dark Star");

    var configId: Int internal set
    var configName: String internal set

    init {
        this.configName = configName
        this.configId = configId
    }

    companion object {
        fun from(findValue: Int): GameQueueConfig {
            try {
                return GameQueueConfig.values().first {
                    it.configId == findValue
                }
            } catch (e: NoSuchElementException) {
                e.printStackTrace()
            }
            return CUSTOM
        }
    }
}