package com.hndsofts.trolol.data.entities.match

/**
 * Created by Rafa on 10/23/2017.
 */

class ParticipantStats {

    var item0: Int = 0
    var item1: Int = 0
    var item2: Int = 0
    var item3: Int = 0
    var item4: Int = 0
    var item5: Int = 0
    var item6: Int = 0
    var kills: Int = 0
    var deaths: Int = 0
    var assists: Int = 0
    var participantId: Int = 0
    var perk0: Int = 0

}