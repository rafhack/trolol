package com.hndsofts.trolol.data.entities

import com.hndsofts.trolol.R

enum class PerkIconReference constructor(perkId: Int, perkIcon: Int) {

    PRESS_THE_ATTACK(8005, R.drawable.pressthreeattacks),
    LETHAL_TEMPO(8008, R.drawable.flowofbattletemp),
    FLEET_FOOTWORK(8021, R.drawable.ksfleetfootwork),
    ELECTROCUTE(8112, R.drawable.tlords),
    PREDATOR(8124, R.drawable.kslycanthropy),
    DARK_HARVEST(8128, R.drawable.kssoulreaper),
    SUMMON_AERY(8214, R.drawable.ksperxie),
    ARCANE_COMET(8229, R.drawable.ksdoomsayer),
    PHASE_RUSH(8230, R.drawable.ksspellslingerssurge),
    GRASP_OF_THE_UNDYING(8437, R.drawable.ksruinedking),
    AFTERSHOCK(8439, R.drawable.veteranvengeance),
    GUARDIAN(8465, R.drawable.ksbuddyshield),
    UNSEALED_SPELLBOOK(8326, R.drawable.ksalchemicalesoterica),
    GLACIAL_AUGMENT(8351, R.drawable.ksglacialaugment),
    KLEPTOMANCY(8359, R.drawable.kslooter);

    var perkId: Int internal set
    var perkIcon: Int internal set

    init {
        this.perkId = perkId
        this.perkIcon = perkIcon
    }

    companion object {
        fun from(findValue: Int): PerkIconReference {
            try {
                return PerkIconReference.values().first { it.perkId == findValue }
            } catch (e: NoSuchElementException) {
                e.printStackTrace()
            }
            return ELECTROCUTE
        }
    }
}