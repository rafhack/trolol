package com.hndsofts.trolol.data.entities.match

/**
 * Created by Rafa on 10/23/2017.
 */
class Match {

    var seasonId: Int = 0
    var queueId: Int = 0
    var gameId: Long = 0
    var participantIdentities: List<ParticipantIdentity> = ArrayList()
    var gameVersion: String = ""
    var platformId: String = ""
    var gameMode: String = ""
    var mapId: Int = 0
    var gameType: String = ""
    var teams: List<TeamStats> = ArrayList()
    var participants: List<MatchParticipant> = ArrayList()
    var gameDuration: Long = 0
    var gameCreation: Long = 0
    var selfParticipantId: Int = 0

}