package com.hndsofts.trolol.data.entities.match

/**
 * Created by Rafa on 10/23/2017.
 */
class MatchReference {

    var lane: String = ""
    var gameId: Long = 0
    var champion: Int = 0
    var platformId: String = ""
    var season: Int = 0
    var queue: Int = 0
    var role: String = ""
    var timestamp: Long = 0

}