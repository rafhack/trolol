package com.hndsofts.trolol.data.entities.staticData

/**
 * Created by Rafa on 5/26/2017.
 */
class Champion {

    var title: String = ""
    var id: Int = 0
    var key: String = ""
    var name: String = ""

}