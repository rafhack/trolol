package com.hndsofts.trolol

import android.app.Application
import com.hndsofts.trolol.data.entities.Region
import com.hndsofts.trolol.data.entities.Summoner

/**
 * Created by Rafa on 5/30/2017.
 */
class TroLOLApplication : Application() {

    val persistanceInstance = Persistance

    override fun onCreate() {
        super.onCreate()
        application = this
    }

    companion object Persistance {
        var region: Region? = null
        var application: TroLOLApplication? = null
        var summoner: Summoner? = null
    }
}