package com.hndsofts.trolol.presentation.summary.championRank

import com.hndsofts.trolol.data.entities.ChampionMastery
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.presentation.base.BaseView

/**
 * Created by Rafa on 10/20/2017.
 */
interface ChampionRankView : BaseView {
    fun onLoadChampAndMasteries(champions: StaticData<Champion>?, masteries: List<ChampionMastery>?)
}