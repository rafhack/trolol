package com.hndsofts.trolol.presentation.currentgame

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.hndsofts.trolol.BuildConfig
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.*
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.MapDetail
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.data.entities.staticData.SummonerSpell
import com.hndsofts.trolol.presentation.base.BaseActivity
import com.hndsofts.trolol.presentation.currentgame.CurrentGameAdapter.Companion.TEAM1
import com.hndsofts.trolol.presentation.currentgame.CurrentGameAdapter.Companion.TEAM2
import com.hndsofts.trolol.presentation.currentgame.bannedchamps.BannedChampionsAdapter
import com.hndsofts.trolol.presentation.currentgame.bannedchamps.BannedChampionsFragment
import com.hndsofts.trolol.presentation.secondToMinute
import com.hndsofts.trolol.presentation.summary.SummaryActivity
import com.hndsofts.trolol.utils.BlurTransformation
import com.hndsofts.trolol.utils.GlideCircleTransform
import kotterknife.bindView
import java.lang.Exception
import java.util.*

class CurrentGameActivity : BaseActivity(), CurrentGameView {

    private var gameInfo: CurrentGameInfo? = null
    private var summoner: Summoner? = null

    val imgCurrentChamp: ImageView by bindView(R.id.activity_current_game_img_summone_current_champ)
    private val imgMapIcon: ImageView by bindView(R.id.activity_current_game_img_map_icon)
    private val tvwMapName: TextView by bindView(R.id.activity_current_game_tvw_map_name)
    private val tvwGameType: TextView by bindView(R.id.activity_current_game_tvw_game_type)
    private val tvwGameLenght: TextView by bindView(R.id.activity_current_game_tvw_game_lenght)
    private val rcvParticipants: RecyclerView by bindView(R.id.activity_current_game_tvw_game_participants)
    private val vpgBans: ViewPager by bindView(R.id.activity_current_game_vpg_bans)

    private var presenter: CurrentGamePresenter = CurrentGamePresenter(this)
    private var adapter: CurrentGameAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_game)

        gameInfo = intent?.extras?.get("GameInfo") as CurrentGameInfo
        summoner = application.persistanceInstance.summoner
        setupView()

        presenter.getMaps()
        presenter.getAllChampionsAndSpells()

        presenter.getLeaguePositions(gameInfo?.participants?.get(0)?.id as Long, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    private fun setupView() {
        adapter = CurrentGameAdapter(gameInfo?.participants)

        adapter?.participantsListener = (View.OnClickListener {
            if (it.tag != 0L)
                presenter.getSummonerById(it.tag as Long)
        })

        rcvParticipants.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        rcvParticipants.adapter = adapter
    }

    private fun separateBans(bans: List<BannedChampion>): Pair<List<BannedChampion>, List<BannedChampion>> {
        val separated: Pair<ArrayList<BannedChampion>, ArrayList<BannedChampion>> = Pair(arrayListOf(), arrayListOf())
        bans.forEach {
            if (it.teamId == TEAM1) separated.first.add(it)
            if (it.teamId == TEAM2) separated.second.add(it)
        }
        return separated
    }

    override fun onLoadChampionsAndSpells(champions: StaticData<Champion>?, spells: StaticData<SummonerSpell>) {
        val selfParticipant: GameParticipant? = gameInfo?.participants?.first { it.id == summoner?.id }
        val champion: Champion? = champions?.data?.get(selfParticipant?.championId)
        val skin: String = Random().nextInt(5).toString()

        adapter?.champions = champions
        adapter?.spells = spells
        adapter?.notifyDataSetChanged()

        Glide.with(imgCurrentChamp.context)
                .load(BuildConfig.STATIC_IMAGE_URL.plus("champion/loading/${champion?.key}_$skin.jpg"))
                .asBitmap()
                .override(imgCurrentChamp.width, imgCurrentChamp.height)
                .transform(BlurTransformation(this, 4f))
                .into(object : BitmapImageViewTarget(imgCurrentChamp) {
                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                        setStatusBarColor(resource)
                        super.onResourceReady(resource, glideAnimation)
                    }

                    override fun onLoadFailed(e: Exception?, errorDrawable: Drawable?) {
                        super.onLoadFailed(e, errorDrawable)
                        Glide.with(imgCurrentChamp.context)
                                .load(BuildConfig.STATIC_IMAGE_URL.plus("champion/loading/${champion?.key}_0.jpg"))
                                .asBitmap()
                                .override(imgCurrentChamp.width, imgCurrentChamp.height)
                                .transform(BlurTransformation(imgCurrentChamp.context, 4f))
                                .into(object : BitmapImageViewTarget(imgCurrentChamp) {
                                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                                        setStatusBarColor(resource)
                                        super.onResourceReady(resource, glideAnimation)
                                    }
                                })
                    }
                })


        if (!gameInfo?.bannedChampions?.isEmpty()!!) {
            val sep: Pair<List<BannedChampion>, List<BannedChampion>> = separateBans(gameInfo?.bannedChampions!!)
            val bans1Fragment = BannedChampionsFragment()
            val bans2Fragment = BannedChampionsFragment()

            bans1Fragment.banneds = sep.first
            bans1Fragment.champions = champions
            bans2Fragment.banneds = sep.second
            bans2Fragment.champions = champions

            val vpAdapter = BannedChampionsAdapter(supportFragmentManager,
                    arrayListOf(bans1Fragment, bans2Fragment))
            vpgBans.adapter = vpAdapter
        }
    }

    override fun onLoadLeaguePositions(summonerId: Long, leaguePositions: List<LeaguePosition>?, nextIndex: Int) {
        adapter?.leagueMaps?.put(summonerId, leaguePositions)
        adapter?.notifyDataSetChanged()

        if (nextIndex < gameInfo?.participants?.size as Int)
            presenter.getLeaguePositions(gameInfo?.participants?.get(nextIndex)?.id as Long, nextIndex)
    }

    override fun onLoadSummoner(summoner: Summoner) {
        val intent = Intent(this, SummaryActivity::class.java)
        application.persistanceInstance.summoner = summoner
        startActivity(intent)
    }

    override fun onLoadMaps(maps: StaticData<MapDetail?>?) {
        val currentMap: MapDetail? = maps?.data?.get(gameInfo?.mapId)

        Glide.with(imgMapIcon.context)
                .load(BuildConfig.STATIC_VERSIONED_URL + "map/${currentMap?.image?.full}")
                .asBitmap()
                .override(imgMapIcon.width, imgMapIcon.height)
                .transform(GlideCircleTransform(imgMapIcon.context as Context))
                .into(imgMapIcon)

        tvwMapName.text = currentMap?.mapName
        tvwGameType.text = GameQueueConfig.from(gameInfo?.gameQueueConfigId?.toInt() as Int).configName

        var time: Long = gameInfo?.gameLength as Long
        val handler = Handler(Looper.getMainLooper())

        handler.post(object : Runnable {
            override fun run() {
                tvwGameLenght.text = time++.secondToMinute()
                handler.removeCallbacks(this)
                handler.postDelayed(this, 1000)
            }
        })
    }

    override fun onError(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    override fun setProgress(progress: Boolean) = if (progress) showProgress() else hideProgress()
}
