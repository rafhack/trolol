package com.hndsofts.trolol.presentation.summary

import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.domain.LeagueInteractor
import com.hndsofts.trolol.domain.StaticDataInteractor
import com.hndsofts.trolol.presentation.base.BasePresenter
import rx.SingleSubscriber
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by Rafhack on 5/26/2017.
 */

class SummaryPresenter(view: SummaryView) : BasePresenter<SummaryView>(view) {

    private val leagueInteractor: LeagueInteractor = LeagueInteractor()
    private val staticDataInteractor: StaticDataInteractor = StaticDataInteractor()

    fun getLeaguePositions(summonerId: Long) {
        view.setProgress(true)
        subscription = leagueInteractor.getLeaguePositions(summonerId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<List<LeaguePosition>>() {
                    override fun onSuccess(value: List<LeaguePosition>?) {
                        view.setProgress(false)
                        view.onLoadLeaguePositions(value)
                    }

                    override fun onError(error: Throwable?) {
                        view.setProgress(false)
                        view.onError(error?.message as String)
                    }
                })
    }

    fun getChampionById(championId: Long) {
        subscription = staticDataInteractor.getChampionById(championId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<Champion>() {
                    override fun onError(error: Throwable?) = view.onError(error?.message as String)

                    override fun onSuccess(value: Champion?) = view.onLoadChampion(value)
                })
    }

}
