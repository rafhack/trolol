package com.hndsofts.trolol.presentation.summonersearch

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.Toast.LENGTH_SHORT
import com.bumptech.glide.Glide
import com.hndsofts.trolol.R
import com.hndsofts.trolol.TroLOLApplication
import com.hndsofts.trolol.data.entities.CurrentGameInfo
import com.hndsofts.trolol.data.entities.Region
import com.hndsofts.trolol.data.entities.Summoner
import com.hndsofts.trolol.data.local.SPHelper
import com.hndsofts.trolol.presentation.base.BaseActivity
import com.hndsofts.trolol.presentation.currentgame.CurrentGameActivity
import com.hndsofts.trolol.presentation.summary.SummaryActivity
import com.hndsofts.trolol.utils.BlurTransformation
import kotterknife.bindView

class SummonerSearchActivity : BaseActivity(), SummonerSearchView {

    //region Fields
    private val presenter: SummonerSearchPresenter = SummonerSearchPresenter(this)
    private val summonerIdKey: String = "SummonerId"

    //region Views
    private val tvwSummoner: EditText by bindView(R.id.activity_summoner_search_edt_summoner_name)
    private val btnSearch: Button by bindView(R.id.activity_summoner_search_btn_search)
    private val imgBackground: ImageView by bindView(R.id.activity_summoner_search_img_background)
    private val spnRegions: Spinner by bindView(R.id.activity_summoner_search_spn_regions)
    //endregion
    //endregion

    //region Lifecycle functions
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summoner_search)
        setupView()
        init()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
    //endregion

    //region Private functions

    private fun init() {
        val sp = SPHelper.getInstance().getPreferences(this)
        if (sp.contains(summonerIdKey)) {
            tvwSummoner.setText(sp.getString(summonerIdKey, ""))
            presenter.getSummonerByName(sp.getString(summonerIdKey, ""))
        }
    }

    private fun setupSpinner() {
        val adapter = RegionAdapter(Region.values())
        spnRegions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) = Unit

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SPHelper.getInstance().getEditor(this@SummonerSearchActivity).putString("region",
                        adapter.regions?.get(position)?.regionName).commit()
                TroLOLApplication.region = adapter.regions?.get(position)
            }
        }
        spnRegions.adapter = adapter
        val defRegion: String = SPHelper.getInstance().getPreferences(this).getString("region", "BR")
        spnRegions.setSelection(Region.values().indexOf(Region.from(defRegion)))
        application.persistanceInstance.region = Region.from(defRegion)
    }

    inner class RegionAdapter(regions: Array<Region>) : ArrayAdapter<Region>(
            this, R.layout.simple_text_view_dropdown) {

        var regions: Array<Region>? = null

        init {
            this.regions = regions
        }

        override fun getCount(): Int = regions?.size as Int

        override fun getItem(position: Int): Region = regions?.get(position) as Region
    }

    private fun setupView() {
        btnSearch.setOnClickListener { presenter.getSummonerByName(tvwSummoner.text.toString()) }

        Glide.with(imgBackground.context)
                .load(R.drawable.tha_rift)
                .asBitmap()
                .transform(BlurTransformation(this, 10f))
                .into(imgBackground)

        setupSpinner()
    }
    //endregion

    //region Overridden functions

    override fun onLoadSummoner(summoner: Summoner) {
        SPHelper.getInstance().getEditor(this).putString(summonerIdKey, summoner.name).commit()
        application.persistanceInstance.summoner = summoner
        presenter.getActiveGame(summoner)
    }

    override fun onNotCurrentPlaying(summoner: Summoner) {
        val intent = Intent(this, SummaryActivity::class.java)
        application.persistanceInstance.summoner = summoner
        startActivity(intent)
    }

    override fun onLoadCurrentGame(currentGameInfo: CurrentGameInfo?, summoner: Summoner) {
        val intent = Intent(this, CurrentGameActivity::class.java)
        intent.putExtra("GameInfo", currentGameInfo)
        startActivity(intent)
    }

    override fun onError(message: String) = Toast.makeText(this, message, LENGTH_SHORT).show()

    override fun setProgress(progress: Boolean) = if (progress) showProgress() else hideProgress()

    //endregion

}
