package com.hndsofts.trolol.presentation.matchdetail

import android.os.Bundle
import com.hndsofts.trolol.presentation.base.BaseActivity

class MatchDetailActivity : BaseActivity(), MatchDetailView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onError(message: String) {

    }

    override fun setProgress(progress: Boolean) {

    }

}