package com.hndsofts.trolol.presentation.base

/**
 * Created by Rafhack on 5/25/2017.
 */

interface BaseView {

    fun onError(message: String)

    fun setProgress(progress : Boolean)

}
