package com.hndsofts.trolol.presentation.summary

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.text.Html
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.hndsofts.trolol.BuildConfig
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.ChampionMastery
import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.Summoner
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.presentation.base.BaseActivity
import com.hndsofts.trolol.presentation.summary.championRank.ChampionRankFragment
import com.hndsofts.trolol.presentation.summary.matchHistory.MatchHistoryFragment
import com.hndsofts.trolol.utils.BlurTransformation
import com.hndsofts.trolol.utils.GlideCircleTransform
import kotterknife.bindView
import me.relex.circleindicator.CircleIndicator
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Rafhack on 5/26/2017.
 */
class SummaryActivity : BaseActivity(), SummaryView, ChampionRankFragment.OnLoadMasteriesListener {

    //region Fields
    //region Views
    private val imgSummonerIcon: ImageView by bindView(R.id.activity_summary_img_summoner_icon)
    private val tvwSummonerName: TextView by bindView(R.id.activity_summary_tvw_summoner_name)
    private val tvwSummonerLevel: TextView  by bindView(R.id.activity_summary_tvw_summoner_level)
    private val imgEloIcon: ImageView  by bindView(R.id.activity_summary_img_league_icon)
    private val imgMainChamp: ImageView  by bindView(R.id.activity_summary_img_summone_main_champ)
    private val tvwLeaguePoints: TextView  by bindView(R.id.activity_summary_tvw_lps)
    private val tvwRank: TextView  by bindView(R.id.activity_summary_tvw_summoner_elo)
    private val tvwLeagueName: TextView  by bindView(R.id.activity_summary_tvw_team)
    private val pieWinRate: PieChart by bindView(R.id.activity_summary_pie_winrate)
    private val imgChmpSquare: ImageView  by bindView(R.id.activity_summary_img_chmp_square)
    private val imgChmpMastery: ImageView  by bindView(R.id.activity_summary_img_chmp_mastery)
    private val tvwMiniSeries: TextView  by bindView(R.id.activity_summary_tvw_series)
    private val vpgFragments: ViewPager by bindView(R.id.activity_summary_vpg_fragments)
    private val ciIndicator: CircleIndicator by bindView(R.id.activity_summary_ci_indicator)
    //endregion

    private val presenter: SummaryPresenter = SummaryPresenter(this)
    private val rankFragment: ChampionRankFragment = ChampionRankFragment()
    private val historyFragment: MatchHistoryFragment = MatchHistoryFragment()
    var summoner: Summoner? = null
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)
        init()
        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
    //endregion

    //region Private
    private fun init() {
        summoner = application.persistanceInstance.summoner
        presenter.getLeaguePositions(summoner!!.id)

        rankFragment.onLoadMasteries = this
        rankFragment.summonerId = summoner!!.id

        val fragments: ArrayList<Fragment> = ArrayList()
        fragments.add(rankFragment)
        fragments.add(historyFragment)

        val adapter = SummaryFragmentAdapter(fragments, supportFragmentManager)
        vpgFragments.adapter = adapter

        ciIndicator.setViewPager(vpgFragments)
    }

    private fun setChart(wins: Int, losses: Int) {
        val entries: ArrayList<PieEntry> = arrayListOf()
        entries.add(PieEntry(losses.toFloat(), "Losses", 2f))
        entries.add(PieEntry(wins.toFloat(), "Wins", 1f))

        val pieDataSet = PieDataSet(entries, "")
        pieDataSet.colors = arrayListOf(Color.parseColor("#f25454"), Color.parseColor("#1e91d3"))
        pieDataSet.valueTextSize = 12f

        val pieData = PieData(pieDataSet)
        pieData.setValueTextColor(Color.WHITE)

        val desc = Description()
        desc.text = "Winrate"
        desc.textColor = Color.WHITE
        desc.textSize = 14f

        pieWinRate.setDrawEntryLabels(false)
        pieWinRate.description = desc
        pieWinRate.data = pieData
        pieWinRate.animateXY(1000, 1200)
        pieWinRate.setHoleColor(Color.TRANSPARENT)
        pieWinRate.invalidate()
    }

    private fun setupView() {
        Glide.with(this)
                .load(BuildConfig.STATIC_VERSIONED_URL + "profileicon/" + summoner?.profileIconId + ".png")
                .asBitmap()
                .transform(GlideCircleTransform(this))
                .into(imgSummonerIcon)

        tvwSummonerLevel.text = getString(R.string.level_x, summoner?.summonerLevel.toString())
        tvwSummonerName.text = summoner?.name
    }

    private fun getTierIcon(tier: String): Int = when (tier) {
        "BRONZE" -> R.drawable.bronze_i
        "SILVER" -> R.drawable.silver_i
        "GOLD" -> R.drawable.gold_i
        "PLATINUM" -> R.drawable.platinum_i
        "DIAMOND" -> R.drawable.diamond_i
        "MASTER" -> R.drawable.master
        "CHALLENGER" -> R.drawable.challenger
        else -> R.drawable.provisional
    }

    private fun Char.getSpannableForSerie(): String {
        val ft = "<font color=\""
        val cft = "</font>"

        return when (this) {
            'N' -> "$ft#AAAAAA\"> &#x2015; $cft"
            'W' -> "$ft#00FF00\"> &#x2713; $cft"
            else -> "$ft#FF0000\"> &#x2718; $cft"
        }
    }
    //endregion

    //region Overridden
    override fun setProgress(progress: Boolean) = if (progress) showProgress() else hideProgress()

    @Suppress("DEPRECATION")
    override fun onLoadLeaguePositions(leaguePosition: List<LeaguePosition>?) {
        leaguePosition?.forEach {
            if (it.queueType == LeaguePosition.QT_SOLO) {
                tvwRank.text = it.tier.toLowerCase().capitalize().plus(" ").plus(it.rank)
                tvwLeagueName.text = it.leagueName
                tvwLeaguePoints.text = getString(R.string.lp, " ${it.leaguePoints}")
                imgEloIcon.setImageResource(getTierIcon(it.tier))
                setChart(it.wins, it.losses)

                var spanned = ""
                it.miniSeries?.progress?.forEach { spanned += it.getSpannableForSerie() }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    tvwMiniSeries.setText(Html.fromHtml(spanned, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE)
                else
                    tvwMiniSeries.setText(Html.fromHtml(spanned), TextView.BufferType.SPANNABLE)
            }
        }
    }

    override fun onLoadChampion(champion: Champion?) {
        val skin: String = Random().nextInt(5).toString()
        System.out.println(skin)
        Glide.with(imgMainChamp.context)
                .load(BuildConfig.STATIC_IMAGE_URL.plus("champion/loading/".plus(champion?.key).plus("_$skin.jpg")))
                .asBitmap()
                .override(imgMainChamp.width, imgMainChamp.height)
                .transform(BlurTransformation(this, 4f))
                .into(object : BitmapImageViewTarget(imgMainChamp) {
                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                        setStatusBarColor(resource)
                        super.onResourceReady(resource, glideAnimation)
                    }

                    override fun onLoadFailed(e: Exception?, errorDrawable: Drawable?) {
                        super.onLoadFailed(e, errorDrawable)
                        Glide.with(imgMainChamp.context)
                                .load(BuildConfig.STATIC_IMAGE_URL.plus("champion/loading/${champion?.key}_0.jpg"))
                                .asBitmap()
                                .override(imgMainChamp.width, imgMainChamp.height)
                                .transform(BlurTransformation(imgMainChamp.context, 4f))
                                .into<BitmapImageViewTarget>(object : BitmapImageViewTarget(imgMainChamp) {
                                    override fun onResourceReady(resource: Bitmap, glideAnimation: GlideAnimation<in Bitmap>?) {
                                        setStatusBarColor(resource)
                                        super.onResourceReady(resource, glideAnimation)
                                    }
                                })
                    }
                })

        Glide.with(imgChmpSquare.context)
                .load(BuildConfig.STATIC_VERSIONED_URL.plus("champion/".plus(champion?.key).plus(".png")))
                .crossFade()
                .override(imgChmpSquare.width, imgChmpSquare.height)
                .transform(GlideCircleTransform(this))
                .into(imgChmpSquare)
    }

    override fun onError(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    override fun onLoad(masteries: List<ChampionMastery>?) {
        val maxMaestry: ChampionMastery? = masteries?.maxBy { it.championLevel }
        if (maxMaestry != null) {
            presenter.getChampionById(maxMaestry.championId)
            when (maxMaestry.championLevel) {
                7 -> imgChmpMastery.setImageResource(R.drawable.chmp_mast_7)
                6 -> imgChmpMastery.setImageResource(R.drawable.chmp_mast_6)
                5 -> imgChmpMastery.setImageResource(R.drawable.chmp_mast_5)
                else -> imgChmpMastery.setImageResource(R.drawable.chmp_mast_5)
            }
        }
    }
    //endregion

}