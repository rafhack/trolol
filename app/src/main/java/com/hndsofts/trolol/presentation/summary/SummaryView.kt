package com.hndsofts.trolol.presentation.summary

import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.presentation.base.BaseView

/**
 * Created by Rafhack on 5/26/2017.
 */
interface SummaryView : BaseView {

    fun onLoadLeaguePositions(leaguePosition: List<LeaguePosition>?)
    fun onLoadChampion(champion: Champion?)
}