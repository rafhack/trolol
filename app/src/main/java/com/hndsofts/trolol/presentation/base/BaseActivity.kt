package com.hndsofts.trolol.presentation.base

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import com.hndsofts.trolol.R
import com.hndsofts.trolol.TroLOLApplication


/**
 * Created by Rafhack on 5/25/2017.
 */
@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private var frmProgress: FrameLayout? = null
    private var frmRoot: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_base)
        frmRoot = findViewById(R.id.activity_base_frm_root)
        frmProgress = findViewById(R.id.activity_base_frm_progress)
    }

    @SuppressLint("InflateParams")
    override fun setContentView(layoutResID: Int) {
        val frmContent: FrameLayout = frmRoot?.findViewById(R.id.activity_base_frm_content)!!
        layoutInflater.inflate(layoutResID, frmContent, true)
        super.setContentView(frmRoot)
    }

    protected fun showProgress() {
        frmProgress?.visibility = VISIBLE
    }

    protected fun hideProgress() {
        frmProgress?.visibility = GONE
    }

    protected fun setStatusBarColor(bitmap: Bitmap?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            Palette.Builder(bitmap).generate({
                window.statusBarColor = it.getDarkVibrantColor(Color.BLACK)
                window.navigationBarColor = it.getDarkVibrantColor(Color.BLACK)
            })
    }

    protected val application: TroLOLApplication
        get() = getApplication() as TroLOLApplication

}