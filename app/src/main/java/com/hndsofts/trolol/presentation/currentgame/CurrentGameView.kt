package com.hndsofts.trolol.presentation.currentgame

import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.Summoner
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.MapDetail
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.data.entities.staticData.SummonerSpell
import com.hndsofts.trolol.presentation.base.BaseView

/**
 * Created by Rafa on 5/31/2017.
 */
interface CurrentGameView : BaseView {

    fun onLoadMaps(maps: StaticData<MapDetail?>?)
    fun onLoadChampionsAndSpells(champions: StaticData<Champion>?, spells: StaticData<SummonerSpell>)
    fun onLoadLeaguePositions(summonerId: Long, leaguePositions: List<LeaguePosition>?, nextIndex: Int)
    fun onLoadSummoner(summoner: Summoner)

}