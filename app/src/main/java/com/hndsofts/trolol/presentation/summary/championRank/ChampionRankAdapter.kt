package com.hndsofts.trolol.presentation.summary.championRank

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hndsofts.trolol.BuildConfig
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.data.entities.ChampionMastery

/**
 * Created by Rafhack on 5/30/2017.
 */
class ChampionRankAdapter(masteries: List<ChampionMastery>?, championLIst: StaticData<Champion>?) :
        RecyclerView.Adapter<ChampionRankAdapter.RankViewHolder>() {

    var champions: StaticData<Champion>? = null
    var masteries: List<ChampionMastery>? = null

    init {
        this.champions = championLIst
        this.masteries = masteries
    }

    override fun onBindViewHolder(holder: RankViewHolder?, position: Int) {
        holder?.bind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RankViewHolder {
        val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.champion_rank_item,
                parent, false)
        return RankViewHolder(view)
    }

    override fun getItemCount(): Int = masteries!!.size


    inner class RankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var tvwChampRank: TextView? = null
        private var tvwChampName: TextView? = null
        private var tvwChampDesc: TextView? = null
        private var tvwChampPnts: TextView? = null
        private var imgChampFace: ImageView? = null
        private var imgChampMast: ImageView? = null

        init {
            tvwChampRank = itemView.findViewById(R.id.champion_rank_item_tvw_rank)
            tvwChampName = itemView.findViewById(R.id.champion_rank_item_tvw_champ_name)
            tvwChampDesc = itemView.findViewById(R.id.champion_rank_item_tvw_champ_desc)
            tvwChampPnts = itemView.findViewById(R.id.champion_rank_item_tvw_champ_points)
            imgChampFace = itemView.findViewById(R.id.champion_rank_item_img_champ)
            imgChampMast = itemView.findViewById(R.id.champion_rank_item_img_mastery)
        }

        fun bind(position: Int) {
            val mastery: ChampionMastery? = masteries?.get(position)
            val champion: Champion? = champions?.data?.get(mastery?.championId)
            tvwChampName?.text = champion?.name
            tvwChampRank?.text = (1 + position).toString()
            tvwChampDesc?.text = champion?.title
            tvwChampPnts?.text = mastery?.championPoints.toString()

            Glide.with(imgChampFace?.context)
                    .load(BuildConfig.STATIC_VERSIONED_URL.plus("champion/".plus(champion?.key).plus(".png")))
                    .crossFade()
                    .into(imgChampFace)

            val mastImage = when (mastery?.championLevel) {
                1 -> R.drawable.m1_champ
                2 -> R.drawable.m2_champ
                3 -> R.drawable.m3_champ
                4 -> R.drawable.m4_champ
                5 -> R.drawable.m5_champ
                6 -> R.drawable.m6_champ
                7 -> R.drawable.m7_champ
                else -> R.drawable.m1_champ
            }
            imgChampMast?.setImageResource(mastImage)
        }

    }

}