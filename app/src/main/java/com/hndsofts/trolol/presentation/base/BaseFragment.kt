package com.hndsofts.trolol.presentation.base

import android.support.v4.app.Fragment
import com.hndsofts.trolol.TroLOLApplication

/**
 * Created by Rafa on 10/23/2017.
 */
open class BaseFragment : Fragment() {

    protected val application: TroLOLApplication
        get() = activity.application as TroLOLApplication

}