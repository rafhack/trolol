package com.hndsofts.trolol.presentation.currentgame.bannedchamps

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by Rafa on 6/7/2017.
 */
class BannedChampionsAdapter(fm: FragmentManager, fragments: ArrayList<Fragment>) : FragmentStatePagerAdapter(fm) {

    private var fragments: ArrayList<Fragment> = arrayListOf()

    init {
        this.fragments = fragments
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size
}