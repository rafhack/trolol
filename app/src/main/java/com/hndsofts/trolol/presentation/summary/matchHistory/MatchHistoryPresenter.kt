package com.hndsofts.trolol.presentation.summary.matchHistory

import com.hndsofts.trolol.data.entities.match.Match
import com.hndsofts.trolol.data.entities.match.MatchReference
import com.hndsofts.trolol.data.entities.staticData.StaticDataPack
import com.hndsofts.trolol.domain.MatchInteractor
import com.hndsofts.trolol.domain.StaticDataInteractor
import com.hndsofts.trolol.presentation.base.BasePresenter
import rx.Single
import rx.SingleSubscriber
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by Rafa on 10/23/2017.
 */
class MatchHistoryPresenter(view: MatchHistoryView) : BasePresenter<MatchHistoryView>(view) {

    private val matchInteractor: MatchInteractor = MatchInteractor()
    private val staticDataInteractor: StaticDataInteractor = StaticDataInteractor()

    private var matchReferences: List<MatchReference> = ArrayList()
    var matches: ArrayList<Match?> = ArrayList()

    fun getStaticData() {
        subscription = Single.zip(
                staticDataInteractor.getAllChampions(),
                staticDataInteractor.getAllSpells(),
                staticDataInteractor.getAllMasteries(),
                staticDataInteractor.getAllItems(),
                { chmps, spls, masts, items -> StaticDataPack(chmps, spls, masts, items) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.onLoadStaticData(it)
                }, { error ->
                    view.onError(error?.message as String)
                })
    }

    fun getRecentMatchList(accountId: Long?) {
        view.setProgress(true)
        subscription = matchInteractor.getRecentMatchList(accountId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<List<MatchReference>>() {
                    override fun onSuccess(value: List<MatchReference>?) {
                        view.setProgress(false)
                        matchReferences = value!!
                        matches.add(null)
                        view.onLoadMatchList(value)
                    }

                    override fun onError(error: Throwable?) {
                        view.setProgress(false)
                        view.onError(error?.message as String)
                    }
                })
    }

    fun loadMatchesOneByOne(currentIndex: Int) {

        if (currentIndex >= matchReferences.size) {
            matches.remove(null)
            view.onFinishLoadingMatches()
            return
        }

        subscription = matchInteractor.getMatchDetail(matchReferences[currentIndex].gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<Match>() {
                    override fun onSuccess(value: Match?) {
                        matches.add(matches.size - 1, value)
                        view.onLoadMatchDetail(value)
                        loadMatchesOneByOne(currentIndex + 1)
                    }

                    override fun onError(error: Throwable?) {
                        loadMatchesOneByOne(currentIndex + 1)
                    }

                })
    }

}