package com.hndsofts.trolol.presentation.summary

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by Rafa on 10/23/2017.
 */
class SummaryFragmentAdapter(fragments: List<Fragment>, fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private var fragments: List<Fragment> = ArrayList()

    init {
        this.fragments = fragments
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

}