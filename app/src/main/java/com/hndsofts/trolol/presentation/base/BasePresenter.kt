package com.hndsofts.trolol.presentation.base

import rx.Subscription

/**
 * Created by Rafhack on 5/25/2017.
 */

open class BasePresenter<V : BaseView>(protected var view: V) {

    protected var subscription: Subscription? = null

    fun detachView() {
        subscription?.unsubscribe()
    }

}
