package com.hndsofts.trolol.presentation.currentgame.bannedchamps

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.BannedChampion
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.presentation.loadWithGlide
import com.hndsofts.trolol.presentation.squareUrl

/**
 * Created by Rafa on 6/7/2017.
 */
class BannedChampionsFragment : Fragment() {

    var banneds: List<BannedChampion>? = null
    var champions: StaticData<Champion>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater?.inflate(R.layout.banned_champions_page_item, container, false)

        val imgBan1: ImageView? = view?.findViewById(R.id.banned_champions_page_item_1)
        val imgBan2: ImageView? = view?.findViewById(R.id.banned_champions_page_item_2)
        val imgBan3: ImageView? = view?.findViewById(R.id.banned_champions_page_item_3)
        val imgBan4: ImageView? = view?.findViewById(R.id.banned_champions_page_item_4)
        val imgBan5: ImageView? = view?.findViewById(R.id.banned_champions_page_item_5)

        imgBan1?.loadWithGlide(champions?.data?.get(banneds?.get(0)?.championId)?.squareUrl())
        imgBan2?.loadWithGlide(champions?.data?.get(banneds?.get(1)?.championId)?.squareUrl())
        imgBan3?.loadWithGlide(champions?.data?.get(banneds?.get(2)?.championId)?.squareUrl())
        imgBan4?.loadWithGlide(champions?.data?.get(banneds?.get(3)?.championId)?.squareUrl())
        imgBan5?.loadWithGlide(champions?.data?.get(banneds?.get(4)?.championId)?.squareUrl())

        return view
    }

}