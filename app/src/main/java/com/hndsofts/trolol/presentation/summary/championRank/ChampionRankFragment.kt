package com.hndsofts.trolol.presentation.summary.championRank

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.ChampionMastery
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData

/**
 * Created by Rafa on 10/20/2017.
 */

class ChampionRankFragment : Fragment(), ChampionRankView {

    private var rcvChampions: RecyclerView? = null
    private var frmProgress: FrameLayout? = null

    private val presenter: ChampionRankPresenter = ChampionRankPresenter(this)
    var summonerId: Long = 0
    var onLoadMasteries: OnLoadMasteriesListener? = null

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onLoadChampAndMasteries(champions: StaticData<Champion>?, masteries: List<ChampionMastery>?) {
        onLoadMasteries?.onLoad(masteries)
        val adapter = ChampionRankAdapter(masteries, champions)
        rcvChampions?.adapter = adapter
    }

    override fun onError(message: String) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    override fun setProgress(progress: Boolean) {
        frmProgress?.visibility = if (progress) VISIBLE else GONE
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater?.inflate(R.layout.base_list_fragment, container, false)

        rcvChampions = view?.findViewById(R.id.base_list_fragment_rcv_list)
        frmProgress = view?.findViewById(R.id.base_list_fragment_frm_progress)

        setupView()
        presenter.getChampsAndMasteries(summonerId)
        return view
    }

    private fun setupView() {
        rcvChampions?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    interface OnLoadMasteriesListener {
        fun onLoad(masteries: List<ChampionMastery>?)
    }

}
