package com.hndsofts.trolol.presentation.summary.matchHistory

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.match.Match
import com.hndsofts.trolol.data.entities.match.MatchReference
import com.hndsofts.trolol.data.entities.staticData.StaticDataPack
import com.hndsofts.trolol.presentation.base.BaseFragment

/**
 * Created by Rafa on 10/23/2017.
 */

class MatchHistoryFragment : BaseFragment(), MatchHistoryView {

    //region Fields
    var presenter: MatchHistoryPresenter = MatchHistoryPresenter(this)
    var adapter: MatchHistoryAdapter = MatchHistoryAdapter(presenter.matches)
    //region Views
    var rcvList: RecyclerView? = null
    var frmProgress: FrameLayout? = null
    //endregion
    //endregion

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater?.inflate(R.layout.base_list_fragment, container, false)

        rcvList = view?.findViewById(R.id.base_list_fragment_rcv_list)
        frmProgress = view?.findViewById(R.id.base_list_fragment_frm_progress)
        setupView()

        presenter.getStaticData()
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    //region Private functions
    private fun setupView() {
        rcvList?.layoutManager = LinearLayoutManager(context, VERTICAL, false)
        rcvList?.adapter = adapter
    }
    //endregion

    //region Overriden functions
    //region MatchHistoryView
    override fun onError(message: String) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    override fun setProgress(progress: Boolean) {
        frmProgress?.visibility = if (progress) VISIBLE else GONE
    }

    override fun onLoadMatchList(matchReferences: List<MatchReference>?) {
        adapter.notifyItemInserted(0)
        presenter.loadMatchesOneByOne(0)
    }

    override fun onLoadMatchDetail(match: Match?) {
        val identity = match?.participantIdentities?.find {
            it.player.summonerId == application.persistanceInstance.summoner?.id
        }
        match?.selfParticipantId = identity?.participantId as Int
        adapter.notifyItemInserted(presenter.matches.size)
    }

    override fun onLoadStaticData(pack: StaticDataPack) {
        adapter.pack = pack
        presenter.getRecentMatchList(application.persistanceInstance.summoner?.accountId)
    }

    override fun onFinishLoadingMatches() {
        adapter.notifyItemRemoved(presenter.matches.size - 1)
    }
    //endregion
    //endregion

}