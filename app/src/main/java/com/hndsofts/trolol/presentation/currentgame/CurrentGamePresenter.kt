package com.hndsofts.trolol.presentation.currentgame

import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.staticData.MapDetail
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.domain.LeagueInteractor
import com.hndsofts.trolol.domain.StaticDataInteractor
import com.hndsofts.trolol.domain.SummonerInteractor
import com.hndsofts.trolol.presentation.base.BasePresenter
import rx.Single
import rx.SingleSubscriber
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by Rafa on 5/31/2017.
 */
class CurrentGamePresenter(view: CurrentGameView) : BasePresenter<CurrentGameView>(view) {

    private val staticDataInteractor: StaticDataInteractor = StaticDataInteractor()
    private val leagueInteractor: LeagueInteractor = LeagueInteractor()
    private val summonerInteractor: SummonerInteractor = SummonerInteractor()

    fun getLeaguePositions(summonerId: Long, index: Int) {
        subscription = leagueInteractor.getLeaguePositions(summonerId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<List<LeaguePosition>>() {
                    override fun onSuccess(value: List<LeaguePosition>?) = view.onLoadLeaguePositions(summonerId, value, index + 1)

                    override fun onError(error: Throwable?) = view.onError(error?.message as String)
                })
    }

    fun getMaps() {
        view.setProgress(true)
        subscription = staticDataInteractor.getMaps()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<StaticData<MapDetail?>>() {
                    override fun onSuccess(value: StaticData<MapDetail?>?) {
                        view.setProgress(false)
                        view.onLoadMaps(value)
                    }

                    override fun onError(error: Throwable?) {
                        view.setProgress(false)
                        view.onError(error?.message as String)
                    }
                })
    }

    fun getAllChampionsAndSpells() {
        subscription = Single.zip(
                staticDataInteractor.getAllChampions(), staticDataInteractor.getAllSpells(),
                { chmps, spls -> Pair(chmps, spls) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ (first, second) ->
                    view.onLoadChampionsAndSpells(first, second)
                }, { error ->
                    view.onError(error?.message as String)
                })
    }

    fun getSummonerById(summonerId: Long) {
        view.setProgress(true)
        subscription = summonerInteractor.getSummonerById(summonerId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setProgress(false)
                    view.onLoadSummoner(it)
                }, {
                    view.setProgress(false)
                    view.onError(it?.message as String)
                })
    }

}