package com.hndsofts.trolol.presentation.summonersearch

import com.hndsofts.trolol.data.entities.CurrentGameInfo
import com.hndsofts.trolol.data.entities.Summoner
import com.hndsofts.trolol.presentation.base.BaseView

/**
 * Created by Rafhack on 5/25/2017.
 */
interface SummonerSearchView : BaseView {

    fun onLoadSummoner(summoner: Summoner)
    fun onNotCurrentPlaying(summoner: Summoner)
    fun onLoadCurrentGame(currentGameInfo: CurrentGameInfo?, summoner: Summoner)
}