package com.hndsofts.trolol.presentation.summary.matchHistory

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.GameQueueConfig
import com.hndsofts.trolol.data.entities.PerkIconReference
import com.hndsofts.trolol.data.entities.match.Match
import com.hndsofts.trolol.data.entities.match.TeamStats
import com.hndsofts.trolol.data.entities.staticData.StaticDataPack
import com.hndsofts.trolol.presentation.loadWithGlide
import com.hndsofts.trolol.presentation.secondToMinute
import com.hndsofts.trolol.presentation.squareUrl


/**
 * Created by Rafa on 10/23/2017.
 */

class MatchHistoryAdapter(private val matches: ArrayList<Match?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var pack: StaticDataPack? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.match_history_item, parent, false))
        } else {
            ProgressHolder(LayoutInflater.from(parent?.context).inflate(R.layout.katarina_progress_item, parent, false))
        }
    }

    override fun getItemCount(): Int = matches.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (getItemViewType(position) == 0)
            (holder as ViewHolder).bind(position)
        else
            (holder as ProgressHolder).bind()
    }

    override fun getItemViewType(position: Int): Int = if (matches[position] == null) 1 else 0

    inner class ProgressHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imgLoad: ImageView = itemView.findViewById(R.id.katarina_progress_item_img)

        fun bind() {
            Glide.with(imgLoad.context)
                    .load(R.drawable.katarina)
                    .asGif()
                    .into(imgLoad)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvwGameType: TextView = itemView.findViewById(R.id.match_history_item_tvw_game_type)
        private val tvwDuration: TextView = itemView.findViewById(R.id.match_history_item_tvw_game_duration)
        private val tvwScore: TextView = itemView.findViewById(R.id.match_history_item_tvw_score)
        private val imgChampion: ImageView = itemView.findViewById(R.id.match_history_item_img_champion)
        private val imgSpell1: ImageView = itemView.findViewById(R.id.match_history_item_img_1_spell1)
        private val imgSpell2: ImageView = itemView.findViewById(R.id.match_history_item_img_1_spell2)
        private val imgPerk: ImageView = itemView.findViewById(R.id.match_history_item_img_mastery)
        private val imgItem0: ImageView = itemView.findViewById(R.id.match_history_item_img_item0)
        private val imgItem1: ImageView = itemView.findViewById(R.id.match_history_item_img_item1)
        private val imgItem2: ImageView = itemView.findViewById(R.id.match_history_item_img_item2)
        private val imgItem3: ImageView = itemView.findViewById(R.id.match_history_item_img_item3)
        private val imgItem4: ImageView = itemView.findViewById(R.id.match_history_item_img_item4)
        private val imgItem5: ImageView = itemView.findViewById(R.id.match_history_item_img_item5)
        private val imgItem6: ImageView = itemView.findViewById(R.id.match_history_item_img_item6)

        fun bind(position: Int) {
            val match = matches[position]
            val selfParticipant = match?.participants?.find { it.participantId == match.selfParticipantId }

            val spell1 = pack?.spells?.data?.get(selfParticipant?.spell1Id)
            val spell2 = pack?.spells?.data?.get(selfParticipant?.spell2Id)

            val item0 = pack?.items?.data?.get(selfParticipant?.stats?.item0?.toLong())
            val item1 = pack?.items?.data?.get(selfParticipant?.stats?.item1?.toLong())
            val item2 = pack?.items?.data?.get(selfParticipant?.stats?.item2?.toLong())
            val item3 = pack?.items?.data?.get(selfParticipant?.stats?.item3?.toLong())
            val item4 = pack?.items?.data?.get(selfParticipant?.stats?.item4?.toLong())
            val item5 = pack?.items?.data?.get(selfParticipant?.stats?.item5?.toLong())
            val item6 = pack?.items?.data?.get(selfParticipant?.stats?.item6?.toLong())

            val win = match?.teams?.find { it.teamId.toLong() == selfParticipant?.teamId } as TeamStats

            val perkIconReference = PerkIconReference.from(selfParticipant?.stats?.perk0 as Int)
            imgPerk.setImageResource(perkIconReference.perkIcon)

            itemView.setBackgroundColor(Color.parseColor(if (win.win == "Win") "#90005000" else "#90500000"))

            tvwGameType.text = GameQueueConfig.from(match.queueId).configName
            tvwDuration.text = match.gameDuration.secondToMinute()

            with(selfParticipant.stats, {
                tvwScore.text = "%d / %d / %d".format(kills, deaths, assists)
            })

            imgChampion.loadWithGlide(pack?.champions?.data?.get(selfParticipant.championId)?.squareUrl())
            imgSpell1.loadWithGlide("${spell1?.image?.group}/${spell1?.image?.full}")
            imgSpell2.loadWithGlide("${spell2?.image?.group}/${spell2?.image?.full}")

            imgItem0.loadWithGlide("${item0?.image?.group}/${item0?.image?.full}")
            imgItem1.loadWithGlide("${item1?.image?.group}/${item1?.image?.full}")
            imgItem2.loadWithGlide("${item2?.image?.group}/${item2?.image?.full}")
            imgItem3.loadWithGlide("${item3?.image?.group}/${item3?.image?.full}")
            imgItem4.loadWithGlide("${item4?.image?.group}/${item4?.image?.full}")
            imgItem5.loadWithGlide("${item5?.image?.group}/${item5?.image?.full}")
            imgItem6.loadWithGlide("${item6?.image?.group}/${item6?.image?.full}")
        }
    }

}