package com.hndsofts.trolol.presentation.summary.championRank

import com.hndsofts.trolol.domain.ChampionMasteryInteractor
import com.hndsofts.trolol.domain.StaticDataInteractor
import com.hndsofts.trolol.presentation.base.BasePresenter
import rx.Single
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by Rafa on 10/20/2017.
 */

class ChampionRankPresenter(view: ChampionRankView) : BasePresenter<ChampionRankView>(view) {

    private val champMasteryInteractor: ChampionMasteryInteractor = ChampionMasteryInteractor()
    private val staticDataInteractor: StaticDataInteractor = StaticDataInteractor()

    fun getChampsAndMasteries(summonerId: Long) {
        view.setProgress(true)
        subscription = Single.zip(
                champMasteryInteractor.getChampionMasteries(summonerId),
                staticDataInteractor.getAllChampions(), { t1, t2 -> Pair(t1, t2) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.setProgress(false)
                    view.onLoadChampAndMasteries(it.second, it.first)
                }, {
                    view.setProgress(false)
                    view.onError(it?.message as String)
                })
    }

}