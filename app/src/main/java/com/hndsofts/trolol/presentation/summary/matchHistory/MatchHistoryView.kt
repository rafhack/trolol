package com.hndsofts.trolol.presentation.summary.matchHistory

import com.hndsofts.trolol.data.entities.match.Match
import com.hndsofts.trolol.data.entities.match.MatchReference
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.data.entities.staticData.StaticDataPack
import com.hndsofts.trolol.data.entities.staticData.SummonerSpell
import com.hndsofts.trolol.presentation.base.BaseView

/**
 * Created by Rafa on 10/23/2017.
 */
interface MatchHistoryView : BaseView {

    fun onLoadMatchList(matchReferences: List<MatchReference>?)

    fun onLoadMatchDetail(match: Match?)

    fun onLoadStaticData(pack: StaticDataPack)

    fun onFinishLoadingMatches()

}