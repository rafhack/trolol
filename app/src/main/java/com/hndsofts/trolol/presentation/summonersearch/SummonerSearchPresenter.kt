package com.hndsofts.trolol.presentation.summonersearch

import com.hndsofts.trolol.data.entities.CurrentGameInfo
import com.hndsofts.trolol.data.entities.Summoner
import com.hndsofts.trolol.domain.SpectatorIInteractor
import com.hndsofts.trolol.domain.SummonerInteractor
import com.hndsofts.trolol.presentation.base.BasePresenter
import rx.SingleSubscriber
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by Rafa on 5/25/2017.
 */

class SummonerSearchPresenter(view: SummonerSearchView) : BasePresenter<SummonerSearchView>(view) {

    private val interactor: SummonerInteractor? = SummonerInteractor()
    private val spectatorInteractor: SpectatorIInteractor = SpectatorIInteractor()

    fun getActiveGame(summoner: Summoner) {
        view.setProgress(true)
        subscription = spectatorInteractor.getActiveGame(summoner.id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<CurrentGameInfo>() {
                    override fun onSuccess(value: CurrentGameInfo?) {
                        view.setProgress(false)
                        view.onLoadCurrentGame(value, summoner)
                    }

                    override fun onError(error: Throwable?) {
                        view.setProgress(false)
                        view.onNotCurrentPlaying(summoner)
                    }
                })
    }

    internal fun getSummonerByName(summonerName: String) {
        view.setProgress(true)
        subscription = interactor!!.getSummonerByName(summonerName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<Summoner>() {
                    override fun onSuccess(value: Summoner) {
                        view.setProgress(false)
                        view.onLoadSummoner(value)
                    }

                    override fun onError(error: Throwable) {
                        view.setProgress(false)
                        view.onError(error.message as String)
                    }
                })
    }

}
