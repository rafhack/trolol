package com.hndsofts.trolol.presentation.currentgame

import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hndsofts.trolol.R
import com.hndsofts.trolol.data.entities.GameParticipant
import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.entities.staticData.Champion
import com.hndsofts.trolol.data.entities.staticData.StaticData
import com.hndsofts.trolol.data.entities.staticData.SummonerSpell
import com.hndsofts.trolol.presentation.loadWithGlide
import com.hndsofts.trolol.presentation.squareUrl

/**
 * Created by Rafa on 6/1/2017.
 */

class CurrentGameAdapter(participants: List<GameParticipant>?) :
        RecyclerView.Adapter<CurrentGameAdapter.CurrentGameViewHolder>() {

    var pairs: SparseArray<ArrayList<GameParticipant>>? = null
    var champions: StaticData<Champion>? = null
    var spells: StaticData<SummonerSpell>? = null
    var leagueMaps: HashMap<Long, List<LeaguePosition>?>? = hashMapOf()
    var participantsListener: View.OnClickListener? = null

    companion object {
        const val TEAM1: Long = 100
        const val TEAM2: Long = 200
    }

    init {
        this.pairs = createDuelists(participants)
    }

    override fun onBindViewHolder(holder: CurrentGameViewHolder?, position: Int) {
        holder?.bind(position)
    }

    override fun getItemCount(): Int = maxOf(pairs?.get(TEAM1.toInt())?.size as Int,
            if (pairs?.get(TEAM2.toInt()) != null) pairs?.get(TEAM2.toInt())?.size!! else 0)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CurrentGameViewHolder {
        val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.game_participant_item,
                parent, false)
        return CurrentGameViewHolder(view)
    }

    inner class CurrentGameViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val tvwSummoner1: TextView = itemView.findViewById(R.id.game_participant_item_tvw_summoner_1)
        private val tvwSummoner2: TextView = itemView.findViewById(R.id.game_participant_item_tvw_summoner_2)
        private val imgChamp1: ImageView = itemView.findViewById(R.id.game_participant_item_img_champ_1)
        private val imgChamp2: ImageView = itemView.findViewById(R.id.game_participant_item_img_champ_2)
        private val tvwChamp1: TextView = itemView.findViewById(R.id.game_participant_item_tvw_champ_1)
        private val tvwChamp2: TextView = itemView.findViewById(R.id.game_participant_item_tvw_champ_2)
        private val tvwTier1: TextView = itemView.findViewById(R.id.game_participant_item_tvw_tier_1)
        private val tvwTier2: TextView = itemView.findViewById(R.id.game_participant_item_tvw_tier_2)
        private val imgTier1: ImageView = itemView.findViewById(R.id.game_participant_item_img_tier_1)
        private val imgTier2: ImageView = itemView.findViewById(R.id.game_participant_item_img_tier_2)
        private val imgSpell1: ImageView = itemView.findViewById(R.id.game_participant_item_img_1_spell1)
        private val imgSpell2: ImageView = itemView.findViewById(R.id.game_participant_item_img_1_spell2)
        private val imgSpell3: ImageView = itemView.findViewById(R.id.game_participant_item_img_2_spell1)
        private val imgSpell4: ImageView = itemView.findViewById(R.id.game_participant_item_img_2_spell2)
        private val view1: View = itemView.findViewById(R.id.game_participant_item_view_1)
        private val view2: View = itemView.findViewById(R.id.game_participant_item_view_2)

        fun bind(position: Int) {
            val team1: ArrayList<GameParticipant>? = pairs?.get(TEAM1.toInt())
            val team2: ArrayList<GameParticipant>? = pairs?.get(TEAM2.toInt())

            tvwSummoner1.text = team1?.getIf(position)?.name
            tvwSummoner2.text = team2?.getIf(position)?.name

            view1.tag = team1?.getIf(position)?.id
            view2.tag = team2?.getIf(position)?.id

            view1.setOnClickListener({ participantsListener?.onClick(it) })
            view2.setOnClickListener({ participantsListener?.onClick(it) })

            if (champions != null) {

                val champ1: Champion? = champions?.data?.get(team1?.getIf(position)?.championId)
                val champ2: Champion? = champions?.data?.get(team2?.getIf(position)?.championId)

                imgChamp1.loadWithGlide(champ1?.squareUrl())
                imgChamp2.loadWithGlide(champ2?.squareUrl())

                tvwChamp1.text = champ1?.name
                tvwChamp2.text = champ2?.name
            }

            if (spells != null) {
                val spell1: SummonerSpell? = spells?.data?.get(team1?.getIf(position)?.spell1Id)
                val spell2: SummonerSpell? = spells?.data?.get(team1?.getIf(position)?.spell2Id)
                val spell3: SummonerSpell? = spells?.data?.get(team2?.getIf(position)?.spell1Id)
                val spell4: SummonerSpell? = spells?.data?.get(team2?.getIf(position)?.spell2Id)

                imgSpell1.loadWithGlide("${spell1?.image?.group}/${spell1?.image?.full}")
                imgSpell2.loadWithGlide("${spell2?.image?.group}/${spell2?.image?.full}")
                imgSpell3.loadWithGlide("${spell3?.image?.group}/${spell3?.image?.full}")
                imgSpell4.loadWithGlide("${spell4?.image?.group}/${spell4?.image?.full}")
            }

            if (leagueMaps != null) {
                if (leagueMaps?.containsKey(team1?.getIf(position)?.id)!!) {
                    leagueMaps?.get(team1?.getIf(position)?.id)?.forEach {
                        if (it.queueType == LeaguePosition.QT_SOLO) {
                            tvwTier1.text = "${it.tier.toLowerCase().capitalize()} ${it.rank}"
                            imgTier1.setImageResource(getTierIcon(it.tier))
                        }
                    }
                }

                if (leagueMaps?.containsKey(team2?.getIf(position)?.id)!!) {
                    leagueMaps?.get(team2?.getIf(position)?.id)?.forEach {
                        if (it.queueType == LeaguePosition.QT_SOLO) {
                            tvwTier2.text = "${it.tier.toLowerCase().capitalize()} ${it.rank}"
                            imgTier2.setImageResource(getTierIcon(it.tier))
                        }
                    }
                }
            }
        }
    }

    private fun List<GameParticipant>.getIf(position: Int): GameParticipant = if (position < this.size) this[position] else GameParticipant()

    private fun getTierIcon(tier: String): Int = when (tier) {
        "BRONZE" -> R.drawable.bronze_i
        "SILVER" -> R.drawable.silver_i
        "GOLD" -> R.drawable.gold_i
        "PLATINUM" -> R.drawable.platinum_i
        "DIAMOND" -> R.drawable.diamond_i
        "MASTER" -> R.drawable.master
        "CHALLENGER" -> R.drawable.challenger
        else -> R.drawable.provisional
    }

    private fun createDuelists(participants: List<GameParticipant>?): SparseArray<ArrayList<GameParticipant>> {
        val pairs = SparseArray<ArrayList<GameParticipant>>()
        participants?.forEach {
            val team: ArrayList<GameParticipant>? = pairs.get(it.teamId.toInt(), arrayListOf())
            team?.add(it)
            pairs.put(it.teamId.toInt(), team)
        }
        return pairs
    }

}