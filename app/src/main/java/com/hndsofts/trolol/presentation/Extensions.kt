package com.hndsofts.trolol.presentation

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.hndsofts.trolol.BuildConfig
import com.hndsofts.trolol.data.entities.staticData.Champion

/**
 * Created by Rafa on 6/7/2017.
 */

fun ImageView.loadWithGlide(url: String?) {
    Glide.with(this.context)
            .load(BuildConfig.STATIC_VERSIONED_URL.plus(url))
            .crossFade()
            .into(this)
}

fun Long.secondToMinute(): String {
    val minutes: Long = this / 60
    val seconds: Long = this % 60
    val timeStr: String = if (minutes < 10) "0$minutes" else minutes.toString()
    return timeStr.plus(if (seconds < 10) ":0$seconds" else ":$seconds")
}

fun Champion.squareUrl(): String = "champion/${this.key}.png"