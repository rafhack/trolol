package com.hndsofts.trolol.domain

import com.hndsofts.trolol.data.entities.staticData.*
import com.hndsofts.trolol.data.remote.ServiceGenerator
import com.hndsofts.trolol.data.remote.services.StaticDataService
import rx.Single

/**
 * Created by Rafa on 5/26/2017.
 */
class StaticDataInteractor {

    private val serviceCached: StaticDataService
        get() = ServiceGenerator.createService(StaticDataService::class.java, useForcedCache = true)

    fun getChampionById(championId: Long): Single<Champion> = serviceCached.getChampionById(championId)

    fun getAllChampions(): Single<StaticData<Champion>> = serviceCached.getAllChampions()

    fun getMaps(): Single<StaticData<MapDetail?>> = serviceCached.getMaps()

    fun getAllSpells(): Single<StaticData<SummonerSpell>> = serviceCached.getAllSpells()

    fun getAllMasteries(): Single<StaticData<Mastery>> = serviceCached.getAllMasteries()

    fun getAllItems(): Single<StaticData<Item>> = serviceCached.getAllItems()
}