package com.hndsofts.trolol.domain

import com.hndsofts.trolol.data.entities.CurrentGameInfo
import com.hndsofts.trolol.data.remote.ServiceGenerator
import com.hndsofts.trolol.data.remote.services.SpectatorService
import rx.Single

/**
 * Created by Rafa on 5/31/2017.
 */
class SpectatorIInteractor {

    private val service: SpectatorService
        get() = ServiceGenerator.createService(SpectatorService::class.java)

    fun getActiveGame(summonerId: Long): Single<CurrentGameInfo> = service.getActiveGame(summonerId)

}