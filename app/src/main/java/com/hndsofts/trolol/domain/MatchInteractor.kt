package com.hndsofts.trolol.domain

import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.hndsofts.trolol.data.entities.match.Match
import com.hndsofts.trolol.data.entities.match.MatchReference
import com.hndsofts.trolol.data.remote.ServiceGenerator
import com.hndsofts.trolol.data.remote.services.MatchService
import rx.Single

/**
 * Created by Rafa on 10/23/2017.
 */
class MatchInteractor {

    private val service: MatchService
        get() = ServiceGenerator.createService(MatchService::class.java, useForcedCache = true)

    fun getRecentMatchList(accountId: Long?): Single<List<MatchReference>> {
        return service.getRecentMatchList(accountId, 20).map { t ->
            val jsonMatchList: JsonArray = t?.get("matches")?.asJsonArray!!
            val matchReferenceList: ArrayList<MatchReference> = ArrayList()
            jsonMatchList.mapTo(matchReferenceList) { GsonBuilder().create().fromJson(it, MatchReference::class.java) }
            matchReferenceList
        }
    }

    fun getMatchDetail(gameId: Long): Single<Match> = service.getMatchDetail(gameId)

}