package com.hndsofts.trolol.domain

import com.hndsofts.trolol.data.entities.LeaguePosition
import com.hndsofts.trolol.data.remote.ServiceGenerator
import com.hndsofts.trolol.data.remote.services.LeagueService
import rx.Single

/**
 * Created by Rafhack on 5/26/2017.
 */
class LeagueInteractor {

    private val service: LeagueService
        get() = ServiceGenerator.createService(LeagueService::class.java, useForcedCache = true)

    fun getLeaguePositions(summoerId: Long): Single<List<LeaguePosition>> = service.getLeaguePosision(summoerId)

}