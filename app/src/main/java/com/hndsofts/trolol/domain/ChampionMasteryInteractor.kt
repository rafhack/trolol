package com.hndsofts.trolol.domain

import com.hndsofts.trolol.data.entities.ChampionMastery
import com.hndsofts.trolol.data.remote.ServiceGenerator
import com.hndsofts.trolol.data.remote.services.ChampionMasteryService
import rx.Single

/**
 * Created by Rafa on 5/26/2017.
 */
class ChampionMasteryInteractor {

    private val service: ChampionMasteryService
        get() = ServiceGenerator.createService(ChampionMasteryService::class.java)


    fun getChampionMasteries(summonerId: Long): Single<List<ChampionMastery>> = service.getChampionMasteries(summonerId)

}